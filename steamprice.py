import math

game_fee = 0.1
steam_fee = 0.05

def round_penny(value):
    # Truncate the value to the lowest penny.
    retval = math.floor(value * 100) / 100.0
    # ensure the rounded value is always at least $0.01
    return retval if retval else 0.01

def sell_price(original_price):
    # Apply fee percents to original price.
    return round_penny(original_price + round_penny(original_price * game_fee) + round_penny(original_price * steam_fee))

def original_price(sp):
    # Determine the user's payment given the sell price.
    op = round_penny(sp / (1 + game_fee + steam_fee))
    # Due to rounding down, the original price might not map perfectly.
    # Add pennies until it does.
    while (sell_price(op) < sp):
        op += 0.01
    return round_penny(op)

def buy_at(av, margin=0.01):
    # Given the average cost, determine the maximum buy price to yield at least
    # the margin in resale.
    # TODO this is probably wrong
    return sell_price(original_price(av) - margin)
