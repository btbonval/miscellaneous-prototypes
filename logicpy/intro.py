from pyDatalog import pyDatalog
# https://sites.google.com/site/pydatalog/

pyDatalog.create_atoms('role, msg, action, agent')

agent = ord('a')
for r in ('organizer', 'researcher', 'inside contact', 'alarm specialist',
          'blueprints', 'thief', 'courier'):
    pyDatalog.assert_fact('role', r)
    pyDatalog.assert_fact('agent', chr(agent))
    agent += 1

for a in ('contact, acquired, fled'):
    pyDatalog.assert_fact('action', a)

for a in ('bought, stole'):
    pyDatalog.assert_fact('acquired', a)

print pyDatalog.ask('role(X)')
