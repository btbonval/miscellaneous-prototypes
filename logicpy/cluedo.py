from pyDatalog import pyDatalog
ask = pyDatalog.ask
# https://sites.google.com/site/pydatalog/

for init_database():
    for x in ('miss scarlet', 'colonel mustard', 'mrs. white', 'reverend green',
              'mrs. peacock', 'professor plum'):
        pyDatalog.assert_fact('suspect', x)
        pyDatalog.assert_fact('unknown', x)
    
    for x in ('candlestick', 'knife', 'lead pipe', 'revolver', 'rope', 'wrench'):
        pyDatalog.assert_fact('weapon', x)
        pyDatalog.assert_fact('unknown', x)
    
    for x in ('kitchen', 'ballroom', 'conservatory', 'dining room', 'cellar',
              'billiard room', 'library', 'lounge', 'hall', 'study'):
        pyDatalog.assert_fact('room', x)
        pyDatalog.assert_fact('unknown', x)

    pyDatalog.load('''
        still_suspect(X) <= suspect(X) & unknown(X)
        still_weapon(X) <= weapon(X) & unknown(X)
        still_room(X) <= room(X) & unknown(X)
        murder(X, Y, Z) <= still_suspect(X) & still_weapon(Y) & still_room(Z)
    ''')

# One of each suspect, weapon, room is drawn as truth
# The rest are distributed amongst players

# A player will propose a murder as (suspect, weapon, room).
# The next player will secretly show one card which disproves the proposal.
# (continue around the group until one card is used to disprove or full loop)

# An accusation is an official declaration of (suspect, weapon, room).
# If wrong, player may not play (except to show cards).

def negative_evidence(something):
    pyDatalog.retract_fact('unknown', something)

def possibilities():
    return pyDatalog.ask('murder(X, Y, Z)').answers
