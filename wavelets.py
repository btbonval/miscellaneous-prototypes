from matplotlib import pyplot
import pywt
from numpysounds import sine_array
from numpysounds import numpy

# Wavelets are good to compress and decompress the sine signals over time. e.g.
# lengthen or shorten the signal. same as iFFT with padding.
# The detail is a (super high) high pass filter and the "a" is a low pass filter

def run_random_sample():
    freq = numpy.random.rand()*440 + 880
    sample = sine_array(hz=freq,n_seconds=0.01)
    noise = numpy.random.rand(len(sample)) - 0.5
    scA, scD = pywt.dwt(sample, 'rbio3.3', mode='sp1')
    ncA, ncD = pywt.dwt(noise, 'rbio3.3', mode='sp1')
    rsample = pywt.idwt(scA, scD, 'rbio3.3', mode='sp1')
    rnoise = pywt.idwt(ncA, ncD, 'rbio3.3', mode='sp1')
    
    slen = min((len(sample), len(rsample)))
    nlen = min((len(noise), len(rnoise)))
    esample = sample[0:slen] - rsample[0:slen]
    enoise = noise[0:nlen] - rnoise[0:nlen]

    return (sample, noise, scA, scD, ncA, ncD, esample, enoise)

if __name__ == '__main__':
    vals = []
    for i in range(0,120):
        vals.append(run_random_sample())
    vals = numpy.array(vals)

    #sample = numpy.mean(vals[:,0])
    sample = vals[5,0]
    # scA averages smoothly (and interesting) across N scA samples of sine waves
    scA = numpy.mean(vals[:,2])
    #scD = numpy.mean(vals[:,3])
    scD = numpy.mean(vals[[0,1],2])
    #esample = numpy.mean(vals[:,6])
    esample = pywt.idwt(scD, numpy.zeros(scD.shape), 'rbio3.3', mode='sp1')
    #noise = numpy.mean(vals[:,1])
    noise = vals[4,2]
    #enoise = numpy.mean(vals[:,7])
    enoise = vals[4,3]
    #ncA = numpy.mean(vals[:,4])
    ncA = pywt.idwt(scA, numpy.zeros(scA.shape), 'rbio3.3', mode='sp1')
    #ncD = numpy.mean(vals[:,5])
    ncD = pywt.idwt(scD, numpy.zeros(scD.shape), 'rbio3.3', mode='sp1')

    vals = []
    for i in range(0,20):
        vals.append(run_random_sample)
    pyplot.subplot(421)
    pyplot.plot(sample)
    pyplot.xlabel('sine')

    pyplot.subplot(422)
    pyplot.plot(esample)
    pyplot.xlabel('sine')

    pyplot.subplot(423)
    pyplot.plot(scA)
    pyplot.xlabel('sine cA')

    pyplot.subplot(424)
    pyplot.plot(scD)
    pyplot.xlabel('sine cD')

    pyplot.subplot(425)
    pyplot.plot(noise)
    pyplot.xlabel('noise')

    pyplot.subplot(426)
    pyplot.plot(enoise)
    pyplot.xlabel('sine')

    pyplot.subplot(427)
    pyplot.plot(ncA)
    pyplot.xlabel('noise cA')

    pyplot.subplot(428)
    pyplot.plot(ncD)
    pyplot.xlabel('noise cD')

    pyplot.show()
