import numpy

from scipy import fft
from pygame import mixer
from pygame import sndarray

sample_rate = 44100
bits = -16
if bits < 0:
    peaks = [-1*2**(abs(bits)-1), 2**(abs(bits)-1)-1]
else:
    peaks = [0, 2**(bits)-1]
mixer.init(frequency=sample_rate, size=bits, channels=1, buffer=256)

def sine_array_onecycle(hz, peak=None):
    if peak is None:
        peak = 2**15-1
    samples = sample_rate / float(hz) # N samples for one cycle
    xvalues = numpy.arange(int(samples)) # generate 0...N
    xvalues = xvalues * (numpy.pi * 2 / samples) # map 0...N onto 0...2*PI
    return (peak * numpy.sin(xvalues)).astype(numpy.int16) # sine wave over X

def sine_array(hz, peak=None, n_seconds=1.0):
    # extend one cycle sine array into the duration specified
    return numpy.resize(sine_array_onecycle(hz, peak), (n_seconds*sample_rate,))

s440 = sine_array(440, peaks[1], 1)
a440 = sndarray.make_sound(s440)
#a440.play()

# http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.cwt.html#scipy.signal.cwt
