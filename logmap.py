# log func

def logistic(R,alpha):
    if (alpha < 0 or alpha > 1):
        print alpha
        raise ArithmeticError('alpha must be bound between 0 and 1 exclusive')
    return R*alpha*(1-alpha)

# things get weird
# 4 < ? R > 3.569946
# N ~= 10**6

def testlog(R, X0, N):
    X = X0
    for i in range(0,N):
        X = logistic(R,X);
    return X
