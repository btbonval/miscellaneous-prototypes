import sys
import numpy

class engine(object):

    def __init__(self, word_list = None):
        if not word_list:
            print 'Input the list of words, each word on its own line, and press Ctrl-D when finished.'
            word_list = map(str.strip, sys.stdin.readlines())
        self.norm_func = str.upper
        self.word_list = map(self.norm_func, word_list)
        self.ord_base = ord(self.norm_func('a'))
        self._build_idx()
        self._process_words()
        self._generate_tree()

    def alpha2idx(self, char):
        return ord(self.norm_func(char)) - self.ord_base

    def idx2alpha(self, idx):
        return chr(idx + self.ord_base)

    def compare_words(self, idx1, idx2):
        word1 = self.word_list[idx1]
        word2 = self.word_list[idx2]
        c = 0
        for i in range(0, len(word1)):
            c += word1[i] == word2[i]

        return c

    def _build_idx(self):
        idx = {}
        for i in range(0, len(self.word_list)):
            idx[self.word_list[i]] = i

        self.find_idx = idx

    def _process_words(self):
        listlength = len(self.word_list)
        wordlength = len(self.word_list[0])
        similarity = numpy.zeros((listlength, listlength))
        for i in range(0, listlength):
            for j in range(0, listlength):
                similarity[i, j] = self.compare_words(i, j)

        self.similarity = similarity

    def _generate_tree(self):
        simmat = self.similarity
        sides = simmat.shape[0]
        matches = numpy.zeros(simmat.shape)
        ones = numpy.ones((1, sides))
        for i in range(0, sides):
            clause = simmat == i
            numinstances = numpy.array([numpy.sum(clause, 0)])
            matches += numinstances.T * ones * clause.astype(numpy.double)
        self.matches = matches

    def find_best(self, given = {}):
        n = self.similarity.shape[0]
        fullidx = numpy.arange(0, n)
        clause = numpy.ones(n, bool)
        for (word, correct,) in given.iteritems():
            clause &= self.similarity[self.find_idx[word], :] == correct
        partidx = fullidx[clause]
        partmat = self.matches[clause, :]

        if partmat.shape[0] == 1:
            idx = [fullidx[partidx[0]]]
            return map(self.word_list.__getitem__, idx)

        sums = numpy.sum(partmat, 1)
        idx = fullidx[partidx[(sums == numpy.min(sums))]]
        suggestion = map(self.word_list.__getitem__, idx)
        return suggestion
